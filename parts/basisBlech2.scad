// basisblech - 2
        
$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

stiftDiameter = 4.5;

use <basics.scad>

blechWidth = cellSize * 8;
blechHeight = cellSize * 7;

module baseisBlech() {
    hull() {
        translate([0         , 0          , 0]) cell();
        translate([blechWidth, 0          , 0]) cell();
        translate([0         , blechHeight, 0]) cell();
        translate([blechWidth, blechHeight, 0]) cell();
    }
    // corner standoffs
    translate([0, 0, layerHeight]) {
        translate([0         , 0          , 0]) cell();
        translate([0         , blechHeight, 0]) cell();
        translate([blechWidth, 0          , 0]) cell();
        translate([blechWidth, blechHeight, 0]) cell();
    }
    // pins on the corner standoffs
    translate([0, 0, layerHeight * 2]) {   
       translate([cellSize / 2              , cellSize / 2              , 0]) pin(pinOffset);
       translate([cellSize / 2              , blechHeight + cellSize / 2, 0]) pin(pinOffset);
       translate([blechWidth + cellSize / 2 , cellSize / 2              , 0]) pin(pinOffset);
       translate([blechWidth + cellSize / 2 , blechHeight + cellSize / 2, 0]) pin(pinOffset);
    } 
}

baseisBlech();