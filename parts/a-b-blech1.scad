// a-b - blech 1

$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

stiftDiameter = 4.5;

use <basics.scad>


abBlechWidth  = 35.0;
abBlechHeight = 15.0;

module abBlech() {
    difference() {
        translate([0, 0, 0]) cube([abBlechWidth, abBlechHeight, layerHeight]);
        hull() {
            translate([cellSize * 0.75, cellSize * 0.5, -0.1]) cell(layerHeight + 0.2);
            translate([cellSize * 0.75, cellSize * 1.5, -0.1]) cell(layerHeight + 0.2);
        }
    }
    translate([cellSize * 4.25, 0, layerHeight]) {
        hull() {
            translate([0, 0       , 0]) cell();
            translate([0, cellSize * 2, 0]) cell();
        }
    }
}

//translate([cellSize * 4.75, cellSize * 2.5, layerHeight]) 
  color("blue") abBlech();
//translate([cellSize * 3.25, cellSize * 2.5, layerHeight]) 
//  mirror([180, 0, 0]) 
//    color("blue") abBlech();


