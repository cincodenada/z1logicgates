// g - Festblech 2

$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

stiftDiameter = 4.5;

use <basics.scad>

blechWidth  = cellSize * 8;
blechHeight = cellSize * 7;

module festBlech() {
    // layer 0
    difference() {
        hull() {
            translate([0         , 0          , 0]) cell(layerHeight);
            translate([blechWidth, 0          , 0]) cell(layerHeight);
            translate([0         , blechHeight, 0]) cell(layerHeight);
            translate([blechWidth, blechHeight, 0]) cell(layerHeight);
        }
        // stift holes
        translate([cellSize * 1.5, cellSize * 3, -0.1])     fourCell();
        translate([cellSize * 5.5, cellSize * 3, -0.1]) fourCell();
        // corner holes for base blech
        translate([cellSize/2, cellSize/2, -0.1])                            pin();
        translate([blechWidth + cellSize/2, cellSize/2, -0.1])               pin();
        translate([cellSize/2, blechHeight + cellSize/2, -0.1])              pin();
        translate([blechWidth + cellSize/2, blechHeight + cellSize/2, -0.1]) pin();
    }
    // layer 1
    translate([0, 0, layerHeight]) {
        // guides a-b blech
        translate([0 , cellSize * 1.5, 0]) {
            hull() {
                translate([0         , 0, 0]) cell();
                translate([blechWidth, 0, 0]) cell();
            }
        }
        translate([0 , cellSize * 4.5, 0]) {
            hull() {
                translate([0         , cellSize, 0]) cell();
                translate([blechWidth, cellSize, 0]) cell();
            }
        }
        

        // base for guides c-d & e-f blech
        difference() {
            hull() {
                translate([0         , 0, 0]) cell();
                translate([blechWidth, 0, 0]) cell();
            }
            translate([cellSize * 0.5, cellSize * 0.5, -0.1]) cylinder(r1 = cellRadius * 1.5, r2 = 0.1, h = layerHeight);
            translate([cellSize * 8.5, cellSize * 0.5, -0.1]) cylinder(r1 = cellRadius * 1.5, r2 = 0.1, h = layerHeight);
        }
        difference() {
            hull() {
                translate([0         , cellSize * 7, 0]) cell();
                translate([blechWidth, cellSize * 7, 0]) cell();
            }
            translate([cellSize * 0.5, cellSize * 7.5, -0.1]) cylinder(r1 = cellRadius * 1.5, r2 = 0.1, h = layerHeight);
            translate([cellSize * 8.5, cellSize * 7.5, -0.1]) cylinder(r1 = cellRadius * 1.5, r2 = 0.1, h = layerHeight);
        }
    }
    
    
    // layer 2
    translate([0, 0, layerHeight * 2]) {
        translate([cellSize * 2.5, cellSize * 2, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 6.5, cellSize * 2, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 2.5, cellSize * 6, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 6.5, cellSize * 6, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        // base for guides c-d & e-f blech
        hull() {
            translate([0         , 0, 0]) cell();
            translate([blechWidth, 0, 0]) cell();
        }
        hull() {
            translate([0         , blechHeight, 0]) cell();
            translate([blechWidth, blechHeight, 0]) cell();
        }
    }
    
    // layer 3
    translate([0, 0, layerHeight * 3]) {
        // centre guide pins e-f blech
        translate([cellSize * 2.5, cellSize * 2, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 6.5, cellSize * 2, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 2.5, cellSize * 6, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        translate([cellSize * 6.5, cellSize * 6, 0]) cylinder(h = layerHeight, d = cellSize * 0.9);
        // side guide pins e-f blech
        translate([cellSize * 0, 0           , 0]) cell();
        translate([cellSize * 0, cellSize * 7, 0]) cell();
        translate([cellSize * 4, 0           , 0]) cell();
        translate([cellSize * 4, cellSize * 7, 0]) cell();
        translate([blechWidth, 0           , 0]) cell();
        translate([blechWidth, cellSize * 7, 0]) cell();
    }    
    // layer 4
    translate([0, 0, layerHeight * 4]) {
        translate([cellSize * 0.5, cellSize * 0.5, 0]) pin(pinOffset);
        translate([cellSize * 0.5, cellSize * 7.5, 0]) pin(pinOffset);        
        translate([cellSize * 8.5, cellSize * 0.5, 0]) pin(pinOffset);
        translate([cellSize * 8.5, cellSize * 7.5, 0]) pin(pinOffset);        
    }
}

module dualHCell() {
    hull() {
        translate([0,        0, 0]) cell();
        translate([cellSize, 0, 0]) cell();
    }
}

module hexHCell() {
    hull() {
        translate([0           , 0, 0]) cell();
        translate([cellSize * 5, 0, 0]) cell();
    }
}

module fourCell() {
    hull() {
        translate([0       , 0       , 0]) cell(layerHeight + 0.2);
        translate([cellSize, 0       , 0]) cell(layerHeight + 0.2);
        translate([0       , cellSize, 0]) cell(layerHeight + 0.2);
        translate([cellSize, cellSize, 0]) cell(layerHeight + 0.2);
    }
}

module stift(height = layerHeight) {
    translate([0, 0, 0]) cylinder(h = height, d = stiftDiameter);
}

color("gray") festBlech();




// modelling stuff only, disable before final render.

/*
abBlechWidth  = 40.0;
abBlechHeight = 15.0;

module abBlech() {
    difference() {
        translate([0, 0, 0]) cube([abBlechWidth, abBlechHeight, layerHeight]);
        hull() {
            translate([cellSize * 0.75, cellSize * 0.5, -0.1]) cell(layerHeight + 0.2);
            translate([cellSize * 0.75, cellSize * 1.5, -0.1]) cell(layerHeight + 0.2);
        }
    }
}

translate([cellSize * 4.75, cellSize * 2.5, layerHeight]) color("blue") abBlech();
translate([cellSize * 3.25, cellSize * 2.5, layerHeight]) mirror([180, 0, 0]) color("blue") abBlech();

cdBlechWidth  = cellSize * 7;
cdBlechHeight = cellSize * 2.9;

module cdBlech() {
    difference() {
        // base blech
        translate([0, 0, 0]) cube([cdBlechWidth, cdBlechHeight, layerHeight]);
        // guide pin holes
        translate([cellSize /2   , cellSize * 1.4, -0.01]) dualXCell(layerHeight + 0.02);
        translate([cellSize * 4.5, cellSize * 1.4, -0.01]) dualXCell(layerHeight + 0.02);
        // swich pin notches
        translate([cellSize * 1, 0.15-0.01, 0]) notch();
        translate([cellSize * 5, 0.15-0.01, 0]) notch();
    }
}

module dualXCell(height = layerHeight) {
    hull() {
        translate([0       , 0, 0]) cell(height);
        translate([cellSize, 0, 0]) cell(height);
    }
}

module notch() {
    translate([0, 0, -0.01]) cell(layerHeight + 0.02);
    translate([0, -0.15, -0.01]) cube([cellSize, cellSize /2, layerHeight + 0.02]);
}

module roundedNotch() {
    difference() {
        union() {
            translate([0, 0, -0.01])  cell(layerHeight + 0.02);
            translate([-cellSize / 2, 0, -0.01]) cube([cellSize * 2, cellSize / 2, layerHeight + 0.02]);
        }
        translate([-cellSize, -0.01, -0.1]) cell(layerHeight + 0.4);
        translate([ cellSize, -0.01, -0.1]) cell(layerHeight + 0.4);
       
    }
}

translate([cdBlechWidth + cellSize / 2, cdBlechHeight + cellSize, layerHeight * 2]) 
    rotate([0, 0, 180]) 
        color("green") cdBlech();
translate([cellSize / 2, cdBlechHeight + cellSize * 1.2, layerHeight * 2]) 
    rotate([0, 0, 0]) 
        color("green") cdBlech();
*/
/*

efBlechWidth = 15.0;
efBlechHeight = 60.0;

module efBlech() {
    difference() {
        translate([0, 0, 0]) cube([efBlechWidth, efBlechHeight, layerHeight]);
        translate([cellSize, cellSize * 2.5, -0.1]) dualYCell(layerHeight + 0.2);
        translate([cellSize, cellSize * 6.5, -0.1]) dualYCell(layerHeight + 0.2);
        translate([cellSize / 2, cellSize * 5, -0.1]) dualXCell(layerHeight + 0.2);
    }
    translate([0, 0           , layerHeight]) tripleXCell();
    translate([0, cellSize * 10, layerHeight]) tripleXCell();
}

module dualYCell(height = layerHeight) {
    hull() {
        translate([0, 0       , 0]) cell(height);
        translate([0, cellSize, 0]) cell(height);
    }
}

module tripleXCell(height = layerHeight) {
    hull() {
        translate([0           , 0, 0]) cell(height);
        translate([cellSize * 2, 0, 0]) cell(height);
    }
}

color("yellow") {
  translate([0, 0, layerHeight * 3]) {
    translate([cellSize, -cellSize, 0]) {
        efBlech();
    }
    translate([cellSize * 5, -cellSize * 2, 0]) {
        efBlech();
    }
  }
}


module dualXCell(height = layerHeight) {
    hull() {
        translate([0       , 0, 0]) cell(height);
        translate([cellSize, 0, 0]) cell(height);
    }
}

module elCell() {
    hull() {
        translate([cellSize * 0, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
        translate([cellSize * 1, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
    }
    hull() {
        translate([cellSize * 0, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
        translate([cellSize * 0, cellSize * 1, -0.1]) cell(layerHeight + 0.2);
    }
}

module cdXORblech() {
    difference() {
        hull() {
            translate([cellSize * 0, cellSize * 0, 0]) cell(layerHeight / 2);
            translate([cellSize * 7, cellSize * 0, 0]) cell(layerHeight / 2);
            translate([cellSize * 0, cellSize * 5, 0]) cell(layerHeight / 2);
            translate([cellSize * 7, cellSize * 5, 0]) cell(layerHeight / 2);
        }
        // L-shapes
        translate([cellSize * 1.5, cellSize * 2.0, 0]) rotate([0, 0, 0]) elCell();
        translate([cellSize * 6.5, cellSize * 4.0, 0]) rotate([0, 0, 180]) elCell();
        
        translate([cellSize * 1.0, cellSize * 0.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 5.0, cellSize * 0.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 1.0, cellSize * 4.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 5.0, cellSize * 4.5, -0.1]) dualXCell(layerHeight + 0.2);
   }
}

translate([cellSize * 1.0, cellSize * 1, layerHeight * 2]) color("green") cdXORblech();
*/