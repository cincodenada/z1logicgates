// e & f - blech 2

$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

use <basics.scad>

efBlechWidth = 15.0;
efBlechHeight = 60.0;

module efBlech() {
    difference() {
        translate([0, 0, 0]) cube([efBlechWidth, efBlechHeight, layerHeight]);
        translate([cellSize, cellSize * 2.5, -0.1]) dualYCell(layerHeight + 0.2);
        translate([cellSize, cellSize * 6.5, -0.1]) dualYCell(layerHeight + 0.2);
        translate([cellSize / 2, cellSize * 5, -0.1]) dualXCell(layerHeight + 0.2);
    }
    translate([0, 0           , layerHeight]) tripleXCell();
    translate([0, cellSize * 10, layerHeight]) tripleXCell();
}

module dualYCell(height = layerHeight) {
    hull() {
        translate([0, 0       , 0]) cell(height);
        translate([0, cellSize, 0]) cell(height);
    }
}

module dualXCell(height = layerHeight) {
    hull() {
        translate([0       , 0, 0]) cell(height);
        translate([cellSize, 0, 0]) cell(height);
    }
}

module tripleXCell(height = layerHeight) {
    hull() {
        translate([0           , 0, 0]) cell(height);
        translate([cellSize * 2, 0, 0]) cell(height);
    }
}

color("yellow") {
//  translate([0, 0, layerHeight * 3]) {
//    translate([cellSize, -cellSize, 0]) {
        efBlech();
//    }
//    translate([cellSize * 5, -cellSize * 2, 0]) {
//        efBlech();
//    }
//  }
}