// topblech - 2
        
$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

stiftDiameter = 4.5;

use <basics.scad>

blechWidth = cellSize * 8;
blechHeight = cellSize * 7;

module topBlech() {
    difference() {
        hull() {
            translate([0         , 0          , 0]) cell();
            translate([blechWidth, 0          , 0]) cell();
            translate([0         , blechHeight, 0]) cell();
            translate([blechWidth, blechHeight, 0]) cell();
        }
        hull() {
            translate([cellSize             , cellSize              , -0.1]) cell(layerHeight + 0.2);
            translate([blechWidth - cellSize, cellSize              , -0.1]) cell(layerHeight + 0.2);
            translate([cellSize             , blechHeight - cellSize, -0.1]) cell(layerHeight + 0.2);
            translate([blechWidth - cellSize, blechHeight - cellSize, -0.1]) cell(layerHeight + 0.2);
        }
        translate([cellSize * 0.5, cellSize * 0.5,-0.1]) pin(pinOffset);
        translate([cellSize * 8.5, cellSize * 0.5,-0.1]) pin(pinOffset);
        translate([cellSize * 0.5, cellSize * 7.5,-0.1]) pin(pinOffset);
        translate([cellSize * 8.5, cellSize * 7.5,-0.1]) pin(pinOffset);
    }

}

topBlech();