// c-d-XOR blech 2

$fn = 16;

cellRadius = 1.0;

cellSize = 5.0;

layerHeight = 2.0;

pinOffset = 0.15;

stiftDiameter = 4.5;

use <basics.scad>

module dualXCell(height = layerHeight) {
    hull() {
        translate([0       , 0, 0]) cell(height);
        translate([cellSize, 0, 0]) cell(height);
    }
}

module elCell() {
    hull() {
        translate([cellSize * 0, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
        translate([cellSize * 1, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
    }
    hull() {
        translate([cellSize * 0, cellSize * 0, -0.1]) cell(layerHeight + 0.2);
        translate([cellSize * 0, cellSize * 1, -0.1]) cell(layerHeight + 0.2);
    }
}

module cdXORblech() {
    difference() {
        hull() {
            translate([cellSize * 0, cellSize * 0, 0]) cell(layerHeight / 2);
            translate([cellSize * 7, cellSize * 0, 0]) cell(layerHeight / 2);
            translate([cellSize * 0, cellSize * 5, 0]) cell(layerHeight / 2);
            translate([cellSize * 7, cellSize * 5, 0]) cell(layerHeight / 2);
        }
        // L-shapes
        translate([cellSize * 1.5, cellSize * 2.0, 0]) rotate([0, 0, 0]) elCell();
        translate([cellSize * 6.5, cellSize * 4.0, 0]) rotate([0, 0, 180]) elCell();
        // guide holes
        translate([cellSize * 1.0, cellSize * 0.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 5.0, cellSize * 0.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 1.0, cellSize * 4.5, -0.1]) dualXCell(layerHeight + 0.2);
        translate([cellSize * 5.0, cellSize * 4.5, -0.1]) dualXCell(layerHeight + 0.2);
   }
}

cdXORblech();