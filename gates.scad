include <parts/basics.scad>
use <parts/a-b-blech1.scad>
use <parts/c-d-AND-blech3.scad>
use <parts/c-d-OR-blech5.scad>
use <parts/c-d-XOR-blech2.scad>
use <parts/e-f-Blech2.scad>
use <parts/g-Festblech2.scad>
use <parts/schaltstift2.scad>
use <parts/topBlech2.scad>
use <parts/basisBlech2.scad>

explode = 1;

//utility
module layer(num) {
  translate([0,0,layerHeight*num*explode])
  children();
}

module shell() {
    festBlech();
    layer(4)
    topBlech();
}

module shift(x, y, anim=true) {
    xshift = anim ? min(1, 2-4*abs($t-0.5)) : 0;
    yshift = anim ? max(0, 1-4*abs($t-0.5)) : 0;
    translate([(x-xshift)*cellSize, (y+yshift)*cellSize, 0])
    children();
}

module flip() {
    translate([0,0,layerHeight])
    mirror([0,0,1])
    children();
}

module rot() {
    mirror([1,0,0])
    children();
}

// Standardizing
module ab() { flip() abBlech(); }
module ef() { flip() efBlech(); }

color("gray") shell();
color("green") shift(1,1,false) layer(2) cdORblech();
color("yellow") shift(1,-1) layer(3) ef();
color("yellow") shift(5,-1) layer(3) ef();
color("blue") shift(4,2.5) layer(1) rot() ab();
color("blue") shift(6,2.5) layer(1) ab();
color("red") shift(3,4.5) layer(-1) switchPin();
color("red") shift(7,4.5) layer(-1) switchPin();

